# dotfiles

## Files

- `home` - installed in `~`;
- `root` - installed in `/etc/`.

## Configs for

- **Helix** editor (new);
- NeoVim editor (old configs);
- `cport-v1.0` ports manager (for Calmira GNU/Linux-libre);
- **Installation script for Trinity DE (old)**.

## Install

- **User:**

```bash
cp -rxva home/* ~
```

- **System:**

```bash
cp -v root/* /etc
```
